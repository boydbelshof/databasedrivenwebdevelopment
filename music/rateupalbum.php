<?php session_start(); ?>
<?php
require_once("../../config/password.inc.php");
require_once("../../config/config.inc.php");
?>
<?php
$value = "1";
$dbh = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
try {
        $qh = $dbh->prepare('INSERT INTO album_votes (album_id, user_id, value) VALUES (?, ?, ?)');
        $qh->execute(array($_GET['albumid'],$_SESSION['user_id'], $value));
} catch (PDOException $e) {
        die("ERROR: {$e->getMessage()}");
}
header('Location: album.php?id='. htmlspecialchars($_GET['albumid']));
?>