<?php session_start(); ?>
<?php $songid = $_GET['id'];?>
<?php include("top.php");?>
<?php
require_once("../../config/password.inc.php");
require_once("../../config/config.inc.php");
?>
<?php
function getImage($url)
{
    $track = $url;
    $url   = "https://embed.spotify.com/oembed/?url=" . $track . "&format=json";
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.x.x) Gecko/20041107 Firefox/x.x");
    $output = curl_exec($ch);
    curl_close($ch);
    
    $get_json = json_decode($output);
    $cover    = $get_json->thumbnail_url;
    return $cover;
}
?>
<?php

		require_once("../../config/config.inc.php");
		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
		}

		$sql1 = "SELECT value FROM song_votes WHERE song_id = $songid";
		$result1 = $conn->query($sql1);
		foreach ($result1 as $row) {
		$value = $row['value'];
		}
		
		$sql = "SELECT id, title, artist, youtube_url, spotify_url, album FROM song WHERE id = '$songid'";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
						echo "<h1>" . $row["artist"]. " - " . $row["title"]. "<br><br>";
						$youtube_url = $row["youtube_url"];
						$spotify_url = $row["spotify_url"];
						$artist = $row["artist"];
		} }
		else {
				echo "0 results";
		}

		$conn->close();
?>
<?php
$track = $spotify_url;
$url = "https://embed.spotify.com/oembed/?url=".$track."&format=json";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.x.x) Gecko/20041107 Firefox/x.x");
$output = curl_exec($ch);
curl_close($ch);

$get_json  = json_decode($output);
$cover     = $get_json->thumbnail_url;
?>
<div class="row">
	<div class="col-md-4">
		<h4>Album Cover</h4>
		<img src='<?php echo $cover; ?>' class="img-responsive">
	</div>
	<div class="col-md-4">
		<h4>Spotify</h4>
		<iframe src="https://embed.spotify.com/?uri=<?php echo $spotify_url; ?>" class="img-responsive" frameborder="0" allowtransparency="true"></iframe>
		<h4>Check Album</h4>
		<?php
			try {
    $db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql3 = 'SELECT s.album, s.artist, a.id FROM song s, albums a WHERE  s.id = ' . $songid . ' AND a.title = s.album AND a.artist = s.artist;';
    foreach ($db->query($sql3) as $row) {
        echo "<a class='btn btn-primary' href='album.php?id=" . $row["id"] . "' >" . $row["artist"] ." - " . $row["album"] . "</a>";
      	
    }
}
catch (PDOException $e) {
    die("ERROR: {$e->getMessage()}");
}
		?>
	</div>
	<div class="col-md-4">
		<h4>Other Tracks By <?php echo $artist; ?></h4>
	<?	try {
    $db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql3 = 'SELECT id, title, artist, spotify_url FROM song WHERE artist = "' . $artist . '" AND id != ' . $songid . ' LIMIT 6';
    foreach ($db->query($sql3) as $row) {
        echo "<div class='col-md-6'>";
        echo "<a href='song.php?id=" . $row["id"] . "' >";
        echo "<img class='img-responsive' src='" . getImage($row["spotify_url"]) . "' style='padding: 5px;'> ";
        echo '</a>';
        echo "</div>";
    }
}
catch (PDOException $e) {
    die("ERROR: {$e->getMessage()}");
}
?>
	</div>
</div>
<div class ="row">
	<div class="col-md-4">

	<?
			if ($value == '1') {
				?>
				<a href=alterdown.php?songid=<?=htmlspecialchars($songid)?> class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-thumbs-down'></span> Dislike</a>	
				<?
			}
		
		elseif ($value == '0') {
			?>
			<a href=alterup.php?songid=<?=htmlspecialchars($songid)?> class='btn btn-xs btn-success'><span class='glyphicon glyphicon-thumbs-up'></span> Like</a>
			<?
		}

		else { ?>

		<a href=rateup.php?songid=<?=htmlspecialchars($songid)?> class='btn btn-xs btn-success'><span class='glyphicon glyphicon-thumbs-up'></span> Like</a>

		<a href=ratedown.php?songid=<?=htmlspecialchars($songid)?> class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-thumbs-down'></span> Dislike</a>	
		<? } ?>

	</div>
</div>
<?php include("bottom.php");?>

