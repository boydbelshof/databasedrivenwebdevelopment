<?php session_start(); ?>
<?php
require_once("../../config/password.inc.php");
require_once("../../config/config.inc.php");
?>
<?php
$value = "1";
$dbh = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
try {
        $qh = $dbh->prepare('INSERT INTO song_votes (song_id, user_id, value) VALUES (?, ?, ?)');
        $qh->execute(array($_GET['songid'],$_SESSION['user_id'], $value));
} catch (PDOException $e) {
        die("ERROR: {$e->getMessage()}");
}
header('Location: song.php?id='. htmlspecialchars($_GET['songid']));
?>	