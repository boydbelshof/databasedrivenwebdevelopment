<?php include("top.php");?>
<?php session_start(); ?>
<?php
require_once("../../config/password.inc.php");
require_once("../../config/config.inc.php");
?>
<?php 
function getImage($url) {
	$track = $url;
	$url = "https://embed.spotify.com/oembed/?url=".$track."&format=json";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.x.x) Gecko/20041107 Firefox/x.x");
	$output = curl_exec($ch);
	curl_close($ch);

	$get_json  = json_decode($output);
	$cover     = $get_json->thumbnail_url;
	return $cover;
}
?>
<?php 
if(!isset($_SESSION["user_id"])) {
	header("Location: ../users");
												} ?>
<?php 
$search = $_GET["keywords"]
?>

<div class="row">
	<h2>Search Results</h2>
<?php 
try {
				$db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = "SELECT * FROM song WHERE title LIKE '%$search%' OR artist LIKE '%$search%'";
    			foreach ($db->query($sql) as $row) {
        				echo "<div class='col-md-3 col-lg-3'>";
						echo "<a href='song.php?id=" . $row["id"]. "' >";
						echo "<img class='img-responsive' src='" .getImage($row["spotify_url"]). "' style='padding: 5px;'> ";
						//echo "</a>";
						echo "</div>";
   				 }
			}
				catch (PDOException $e) {
        			die("ERROR: {$e->getMessage()}");
						}

			?>
<?php include("bottom.php");?>
