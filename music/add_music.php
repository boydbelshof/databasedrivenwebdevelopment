<?php include("top.php");?>
<?php session_start(); ?>
<?php 
if(!isset($_SESSION["user_id"])) {
	header("Location: ../users");
												} ?>



<div class="row">

<div class="col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">

		<form class="form-horizontal" action="add.php" method="POST">
      <h2>Add Music</h2>
<fieldset>

<div class="form-group">

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="artist">Artist</label>  
  <div class="col-md-4">
  <input id="artist" name="artist" placeholder="The Beatles" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="title">Song Title</label>  
  <div class="col-md-4">
  <input id="title" name="title" placeholder="Yellow Submarine" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="album">Album Name</label>  
  <div class="col-md-4">
  <input id="album" name="album" placeholder="Greatest Hits vol. 1" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="spotify">Spotify URI</label>  
  <div class="col-md-5">
  <input id="spotify" name="spotify" placeholder="spotify:track:62d2bS1weWhdHZH1lSpJp8" class="form-control input-md" type="text">
  <span class="help-block">Please enter the Spotify track URI</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="youtube">Youtube URL</label>  
  <div class="col-md-5">
  <input id="youtube" name="youtube" placeholder="http://youtube.com/" class="form-control input-md" type="text">
  <span class="help-block">Please enter the youtube track url</span>  
  </div>
</div>

<input type="submit" name="submit" value="Add Song" class="btn btn-primary">

</fieldset>
</form>


</div>
<?php include("bottom.php");?>
