		<?
		include("top.php");

		session_start();
		if (!isset($_SESSION['user_id'])) {
		die('ERROR: not logged in');
		}			
		$id = $_SESSION['user_id'];
				
		require_once('../../config/config.inc.php');	
		require_once("../../config/password.inc.php");

		$dbh = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$qh = $dbh->prepare("SELECT date_of_birth, name FROM users WHERE user_id=$id");
		$qh->execute(array($_GET['date_of_birth'], $_GET['name']));


		foreach ($qh as $row) {
			$name = $row['name'];
			$date =  $row["date_of_birth"];
		}

		?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset=UTF-8>
		<title>Edit user settings</title>
		<link rel=stylesheet href=style.css>
	</head>
	<body>

<div class="row">
	
<div class="col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
<h2>Edit user settings</h2>
	<form class="form-horizontal" action="edit_account.php" method="POST">
	<input type=hidden name=user_id value=<?php echo $_SESSION['user_id'];?> >
	<fieldset>


	<div class="form-group">
  <label class="col-md-4 control-label" for="name">Name</label>  
  <div class="col-md-5">
  <input id="name" name="name"  class="form-control input-md" type="text" value=<?= htmlspecialchars($name) ?>> 
  <span class="help-block">Please enter name if you wish to change it </span>  
  </div>
</div>

	<div class="form-group">
  <label class="col-md-4 control-label" for="name">Name</label>  
  <div class="col-md-5">
  <input id="date_of_birth" name="date_of_birth"  class="form-control input-md" type="text" value=<?= htmlspecialchars($date) ?>> 
  <span class="help-block">Please enter your date of birth if you wish to change it</span>  
  </div>
</div>


		<p><input type=submit value="Submit" class="btn btn-primary"></p>
</fieldset>
</form>
</div>
</div>
	</body>
</html>

<?php include("bottom.php");?>

