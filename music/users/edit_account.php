<?php session_start(); ?>
<?php
require_once("../../config/password.inc.php");
require_once("../../config/config.inc.php");
?>
<?php 
$dbh = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$id = $_SESSION['user_id'];

try {
        $qh = $dbh->prepare('UPDATE users SET name =  ?, date_of_birth = ? WHERE user_id = ?');
        $qh->execute(array($_POST['name'],$_POST['date_of_birth'],$_POST['user_id']));
} catch (PDOException $e) {
        die("ERROR: {$e->getMessage()}");
}
header('Location: account.php?id='. htmlspecialchars($_SESSION['user_id']));
?>

