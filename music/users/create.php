<?php
require_once("../../config/password.inc.php");
require_once("../../config/config.inc.php");
$password_hash = password_hash($_POST['Password'], PASSWORD_DEFAULT);
		$dbh = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
try {
        $qh = $dbh->prepare('INSERT INTO users (name, date_of_birth, password_hash) VALUES (?, ?, ?)');
        $qh->execute(array($_POST['Username'],$_POST['dob'], $password_hash));
} catch (PDOException $e) {
        die("ERROR: {$e->getMessage()}");
}

header('Location: index.php');
?>
