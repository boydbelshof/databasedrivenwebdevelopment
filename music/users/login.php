<?php
require_once("../../config/config.inc.php");
require_once("../../config/password.inc.php");
try {
        $dbh = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $qh = $dbh->prepare('SELECT user_id, password_hash FROM users WHERE name = ?');
        $qh->execute(array($_POST['Username']));

        foreach ($qh as $row) {
                if (password_verify($_POST['Password'], $row['password_hash'])) {
                        session_start();
                        $_SESSION['user_id'] = $row['user_id'];
                        header('Location: index.php');
                        die();
                } else {
                        die('ERROR: wrong password');
                }
        }
} catch (PDOException $e) {
        die('ERROR: ' . htmlspecialchars($e->getMessage()));
}

die('ERROR: unkown user name');

?>
