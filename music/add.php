<?php session_start(); ?>
<?php
require_once("../../config/password.inc.php");
require_once("../../config/config.inc.php");
?>
<?php 
$dbh = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
try {
        $qh = $dbh->prepare('INSERT INTO song (title, artist, youtube_url, spotify_url) VALUES (?, ?, ?, ?)');
        $qh->execute(array($_POST['title'],$_POST['artist'], $_POST['youtube'],$_POST['spotify']));
} catch (PDOException $e) {
        die("ERROR: {$e->getMessage()}");
}

?>
