<?php include("top.php");?>
<?php 
			$page = intval($_GET['page']);
			if($page < 0){
				$page = 1;
			}
			$nextpage = $page+1;
			$nextpage = (string)$nextpage;
			if($page > 0) {
			$previouspage = $page-1; }
			$previouspage = (string)$previouspage;
?>
<?php 
function getImage($url) {
	$track = $url;
	$url = "https://embed.spotify.com/oembed/?url=".$track."&format=json";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.x.x) Gecko/20041107 Firefox/x.x");
	$output = curl_exec($ch);
	curl_close($ch);

	$get_json  = json_decode($output);
	$cover     = $get_json->thumbnail_url;
	return $cover;
}
?>
<div class="row">
<h1>Discover</h1>
<hr>
<h4>Songs based on your likes</h4>
<?php
		$number = $page * 20;
		$offset = (string)$number;
		$index = 0;
		require_once("../../config/config.inc.php");
		
		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {


				die("Connection failed: " . $conn->connect_error);
		}
		
		$sql = "SELECT id, title, artist, spotify_url FROM song LIMIT 20 OFFSET $offset";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
						echo "<div class='col-md-3'>";
						echo "<a href='song.php?id=" . $row["id"]. "' >";
						echo "<img class='img-responsive' src='" .getImage($row["spotify_url"]). "' style='padding: 5px;'> ";
						//echo "</a>";
						echo "</div>";
						$index = $index + 1;
		} }
		else {
				echo "0 results";
		}
		$conn->close();
?>
</div>
<div class="row">
<center>
<a href='discover.php?page=<?php echo $previouspage; ?>' class="btn btn-success">Last Page</a>
<a href='discover.php?page=<?php echo $nextpage; ?>' class="btn btn-success">Next Page</a>
</center>
</div>
<?php include("bottom.php");?>
