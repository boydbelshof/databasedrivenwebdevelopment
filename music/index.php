<?php
include("home_top.php");
?>
<?php
require_once("../../config/password.inc.php");
require_once("../../config/config.inc.php");
?>
<?php
session_start();
?>
<?php
function getImage($url)
{
    $track = $url;
    $url   = "https://embed.spotify.com/oembed/?url=" . $track . "&format=json";
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.x.x) Gecko/20041107 Firefox/x.x");
    $output = curl_exec($ch);
    curl_close($ch);
    
    $get_json = json_decode($output);
    $cover    = $get_json->thumbnail_url;
    return $cover;
}
?>
               <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <h1>Discover Music</h1>
                                <h3>Most Popular</h3>
                                <div class="row">
            

            <?php
try {
    $db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = 'SELECT s.id, s.title, s.artist, s.spotify_url, t.song_id, COUNT(t.value) AS "count" FROM song s, song_votes t WHERE s.id = t.song_id GROUP BY t.song_id LIMIT 8';
    foreach ($db->query($sql) as $row) {
        echo "<div class='col-md-3'>";
        echo "<a href='song.php?id=" . $row["id"] . "' >";
        echo "<img class='img-responsive' src='" . getImage($row["spotify_url"]) . "' style='padding: 5px;'> ";
        echo '</a>';
        echo "</div>";
    }
}
catch (PDOException $e) {
    die("ERROR: {$e->getMessage()}");
}
?>
</div>
               <br> <h3>Recently Added</h3><div class="row">
            <?php
try {
    $db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = 'SELECT s.id, s.title, s.artist, s.spotify_url FROM song s ORDER BY s.id DESC LIMIT 8';
    foreach ($db->query($sql) as $row) {
        echo "<div class='col-md-3'>";
        echo "<a href='song.php?id=" . $row["id"] . "' >";
        echo "<img class='img-responsive' src='" . getImage($row["spotify_url"]) . "' style='padding: 5px;'> ";
        echo "</a>";
        echo "</div>";
    }
}
catch (PDOException $e) {
    die("ERROR: {$e->getMessage()}");
}

?></div>

                    <?php
if (isset($_SESSION["user_id"])) {
    echo "<br><h3>Your Favorites</h3><div class='row'>";
    
    try {
        $db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'SELECT s.id, s.title, s.artist, s.spotify_url, users.user_id, sv.user_id, s.id, sv.song_id FROM song s, song_votes sv, users WHERE sv.user_id = users.user_id AND s.id = sv.song_id AND sv.value = 1 AND users.user_id =' . $_SESSION["user_id"] . ' ORDER BY s.id ASC LIMIT 8';
        foreach ($db->query($sql) as $row) {
            echo "<div class='col-md-3'>";
            echo "<a href='song.php?id=" . $row["id"] . "' >";
            echo "<img class='img-responsive' src='" . getImage($row["spotify_url"]) . "' style='padding: 5px;'> ";
            echo "</a>";
            echo "</div>";
        }
    }
    catch (PDOException $e) {
        die("ERROR: {$e->getMessage()}");
    }
    
}

?>            
             </div>           
            </div>
        </div>

<?php
include("home_bottom.php");
?>