<?php include("top.php");?>
<?php session_start(); ?>
<?php 
function getImage($url) {
	$track = $url;
	$url = "https://embed.spotify.com/oembed/?url=".$track."&format=json";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.x.x) Gecko/20041107 Firefox/x.x");
	$output = curl_exec($ch);
	curl_close($ch);

	$get_json  = json_decode($output);
	$cover     = $get_json->thumbnail_url;
	return $cover;
}
?>
<?php 
if(!isset($_SESSION["user_id"])) {
	header("Location: ../users");
												} ?>

<div class="row">
		<h2>My Latest Songs</h2>
<?php
		$userid = $_SESSION["user_id"];
		$i = 0;
		$index = 0;
		$voted_songs = array();
		require_once("../../config/config.inc.php");
		
		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
		}

		$sql = "SELECT song_id, user_id, value FROM song_votes WHERE user_id = '$userid'";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
				    array_push($voted_songs,$row["song_id"]);
		} }
		else {
				echo "0 results";
		}
		while($i < count($voted_songs)){
				$current_id = $voted_songs[$i];
				$sql = "SELECT id, title, artist, spotify_url FROM song WHERE id = '$current_id'";
				$result = $conn->query($sql);

				if ($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
								echo "<div class='col-md-3'>";
								echo "<a href='song.php?id=" . $current_id . "' >";
								//echo "<p>Song title: " . $row["title"]. "<br>Artist: " . $row["artist"]. "<p>";
								echo "<img class='img-responsive' src='" .getImage($row["spotify_url"]). "' style='padding: 5px;'> ";
								echo "</a>";
								echo "</div>";
								$index = $index + 1;
				} }
				else {
						echo "0 results";
				}
				$i = $i + 1;
		
		}

		$conn->close();
?>
</div>

<div class="row">
		<h2>My Latest Albums</h2>
<?php
		$userid = $_SESSION["user_id"];
		$i = 0;
		$index = 0;
		$voted_albums = array();
		require_once("../../config/config.inc.php");
		
		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
		}

		$sql = "SELECT album_id, user_id, value FROM album_votes WHERE user_id = '$userid'";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
				    array_push($voted_albums,$row["album_id"]);
		} }
		else {
				echo "0 results";
		}
		while($i < count($voted_albums)){
				$current_id = $voted_albums[$i];
				$sql = "SELECT id, title, artist, spotify_url FROM albums WHERE id = '$current_id'";
				$result = $conn->query($sql);

				if ($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
								echo "<div class='col-md-3'>";
								echo "<a href='album.php?id=" . $current_id . "' >";
								//echo "<p>Song title: " . $row["title"]. "<br>Artist: " . $row["artist"]. "<p>";
								echo "<img class='img-responsive' src='" .getImage($row["spotify_url"]). "' style='padding: 5px;'> ";
								echo "</a>";
								echo "</div>";
								$index = $index + 1;
				} }
				else {
						echo "0 results";
				}
				$i = $i + 1;
		
		}

		$conn->close();
?>
</div>

<?php include("bottom.php");?>
