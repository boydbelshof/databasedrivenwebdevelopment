CREATE TABLE `users` (
`user_id` INT NOT NULL AUTO_INCREMENT ,
`name` VARCHAR(255) NOT NULL ,
`date_of_birth` DATE NOT NULL ,
`registration_date` DATE NOT NULL ,
`password_hash` CHAR(60) NOT NULL ,
PRIMARY KEY (`user_id`));

CREATE TABLE `song` ( 
`id` INT NOT NULL AUTO_INCREMENT , 
`title` VARCHAR(255) NOT NULL , 
`artist` VARCHAR(255) NOT NULL , 
`release_date` DATE NOT NULL ,
`youtube_url` VARCHAR(255) NOT NULL ,
`spotify_url` VARCHAR(255) NOT NULL , 
PRIMARY KEY (`id`));

CREATE TABLE `albums` ( 
`id` INT NOT NULL AUTO_INCREMENT , 
`title` VARCHAR(255) NOT NULL , 
`artist` VARCHAR(255) NOT NULL , 
`release_date` DATE NOT NULL ,
`youtube_url` VARCHAR(255) NOT NULL ,
`spotify_url` VARCHAR(255) NOT NULL , 
PRIMARY KEY (`id`));

CREATE TABLE `song_votes` ( 
`id` INT NOT NULL AUTO_INCREMENT , 
`song_id` VARCHAR(255) NOT NULL , 
`user_id` VARCHAR(255) NOT NULL , 
`value` VARCHAR(255) NOT NULL , 
PRIMARY KEY (`id`));

CREATE TABLE `album_votes` ( 
`id` INT NOT NULL AUTO_INCREMENT , 
`album_id` VARCHAR(255) NOT NULL , 
`user_id` VARCHAR(255) NOT NULL , 
`value` VARCHAR(255) NOT NULL , 
PRIMARY KEY (`id`));
